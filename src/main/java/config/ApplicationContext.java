package config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import tamarak.core.entity.Bid;
import tamarak.core.entity.Customer;
import tamarak.core.entity.Project;

/**
 * Java based application context configuration class.
 * @param <K>
 * @param <V>
 *
 */
@Configuration
@ComponentScan(value = "tamarak")
public class ApplicationContext<K, V>
{
	// Creating an in-memory data container
	@Bean
	public Map<String, Map<K,V>> dataCache(){
		Map<String, Map<K,V>> cacheMap = new HashMap<String, Map<K, V>>();
		cacheMap.put("projects",  new HashMap<K, V> ());
		cacheMap.put("customers", new HashMap<K, V>());
		cacheMap.put("bids",new HashMap<K, V>());
		return cacheMap;
	}
	
    
    @Bean
    public Map<Integer, Project> projects() {
        final Map<Integer, Project> projectMap = new HashMap<>();
        return projectMap;
    }
    
    @Bean
    public Map<Integer, List<Bid>> bids() {
        final Map<Integer, List<Bid>> bidMap = new HashMap<>();
        return bidMap;
    }
    
    @Bean
    public Map<Integer, Customer> customers() {
        final Map<Integer, Customer> customerMap = new HashMap<>();
        return customerMap;
    }
}
