package config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * This Class is used to intercept all the requests.  Usually login is excluded.
 * 
 * 
 * 
 */
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {
	private final static Logger LOG = LoggerFactory.getLogger(AuthenticationInterceptor.class);

	public AuthenticationInterceptor() {}

	/**
	 * This method handles all the request for validating the session and auth
	 * token
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
			return createSession(request, response);			
	}

	/**
	 * Create session 
	 * @param request
	 * @param response
	 * @return boolean
	 */
	private boolean createSession(HttpServletRequest request, HttpServletResponse response){
		try {
			HttpSession session = request.getSession(true);
			if(session.getAttribute("clientId")==null){
				// TODO: This is for the sake of creating a unique clientId but
				// it is not full proof, it s/b auto generated to enforce uniqueness
				long timestamp = System.currentTimeMillis();
				session.setAttribute("clientId", Integer.valueOf((int)timestamp/1000));
			}
		}	
		catch(Exception e){
			return false;
		}
		return true;

	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		LOG.info("Interceptor invoked For Post");
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		LOG.info("Interceptor invoked Complete");
	}
}
