package tamarak.common;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import tamarak.core.model.ProjectModel;


public final class MarshallingUtil {

	private static ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
		return mapper;
	}

	public static String marshalToJson(Object o) {

		ObjectMapper mapper = getObjectMapper(); // can reuse, share globally

		String str = null;
		try {
			str = mapper.writeValueAsString(o);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return str;
	}

	@SuppressWarnings("unchecked")
	public static Object unmarshalFromJson(String s, Class c) {

		ObjectMapper mapper = getObjectMapper(); // can reuse, share globally

		Object o = null;
		try {
			o = mapper.readValue(s, c);
			System.out.println(o.toString());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return o;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ProjectModel pm = new ProjectModel();
		pm.setBuyerBid(101d);
		pm.setBuyerPassword("MickyMouse");
		pm.setBuyerEmail("mm@gmail.com");
		pm.setCurrency("$");
		pm.setId(1);
		System.out.println(MarshallingUtil.marshalToJson(pm));
	}

}
