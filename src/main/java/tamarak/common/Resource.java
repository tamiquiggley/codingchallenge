package tamarak.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This class is common class for Reponse JSON message format.
 * @author tmcdonald
 *
 * @param <T>
 */

@JsonInclude(Include.NON_NULL)  
public class Resource<T> {
	
	private T data;
	private boolean success = true;
	
	//Introducing the dummy constructor
    public Resource() {
    }

	public Resource(T entity) {
		this.data = entity;
	}

	public Resource(boolean status, T entity) {
		this.success = status;
		this.data = entity;

	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}


	public boolean isSuccess() {
		return success;
	}


	public void setSuccess(boolean success) {
		this.success = success;
	}



}