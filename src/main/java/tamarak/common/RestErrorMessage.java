package tamarak.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This class is common for REST API error response in JSON format
 * @author tmcdonald
 *
 */
@JsonInclude(Include.NON_NULL)
public class RestErrorMessage {

    private String error;
    private String message;
    private List<String> errors = new ArrayList<>();
    
    public RestErrorMessage() {}
	 
    public RestErrorMessage(String error, String message) {
       	this.error = error;
        this.message = message;
    }
 
    public RestErrorMessage(String error,  List<String> errors) {
    	this.error = error;
    	this.errors = errors;
  
    }

	public String getError() {
		return error;
	}

	public String getMessage() {
		return message;
	}

	public List<String> getErrors() {
		return errors;
	}

}
