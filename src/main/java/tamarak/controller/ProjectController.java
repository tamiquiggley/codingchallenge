package tamarak.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import tamarak.common.RestErrorMessage;

import tamarak.common.MarshallingUtil;
import tamarak.common.Resource;
import tamarak.core.entity.Bid;
import tamarak.core.entity.Project;
import tamarak.core.model.ProjectModel;
import tamarak.core.service.ProjectService;
import tamarak.core.service.impl.BiddingOpenException;


/**
 *
 * A sample controller class for a classic bidding system 
 *
 * @author TMcDonald
 */
@Controller
public class ProjectController
{

    private static final Logger LOG = LoggerFactory.getLogger(ProjectController.class.getName());

    @Autowired
    private ProjectService projectService;
    
    private static String VALIDATION_ERROR = "101";
    private static String BIDDING_STILL_OPEN_ERROR = "102";
    /**
     * Create project with maximum budget, bid deadline days and description.
     * @param 
     * @return project Id
     */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/project", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Resource> updateOrCreateProject(@RequestBody ProjectModel requestProject, 
			HttpSession session) throws Exception {
		LOG.info("Request payload:" + MarshallingUtil.marshalToJson(requestProject));
		HttpStatus httpStatus;
		//
		// Validations
		//
		List<String> errors = new ArrayList<String>();
		String error = null;
		if(requestProject.getCurrency() == null || 
				(!requestProject.getCurrency().equals("$") 
						&& !requestProject.getCurrency().toLowerCase().equals("dollars"))){
			error = "Currency must be $ or dollars.";
			LOG.error(error);
			errors.add(error);
		}
		if(requestProject.getMaxBudget() == null || requestProject.getMaxBudget().doubleValue() < ProjectService.MIN_BID){
			error = "Budget must not be null and 100 dollars or more.";
			LOG.error(error);
			errors.add(error);
		}
		if(requestProject.getBidOpenDays() == null ){
			error = "Days open for bidding.";
			LOG.error(error);
			errors.add(error);
		}
		if (requestProject.getDescription().isEmpty()){
			error = "Description must not be null.";
			LOG.error(error);
			errors.add(error);
		}
		if(errors.size() > 0){
			RestErrorMessage errorMessage = new RestErrorMessage(VALIDATION_ERROR, errors);
			Resource<RestErrorMessage> resource = new Resource<RestErrorMessage>(false, errorMessage);
			return new ResponseEntity<Resource>(resource, HttpStatus.BAD_REQUEST);
		} 
		Project project = null;
		try {
			project = projectService.save(requestProject.getId(),requestProject.getMaxBudget(), 
					Long.valueOf(formulateTimestamp(requestProject.getBidOpenDays().intValue())), 
					requestProject.getDescription());
			if(project.getId() != null){
				httpStatus = HttpStatus.OK;
			}else {
				httpStatus = HttpStatus.NOT_IMPLEMENTED;
			}
		}catch(Exception e){
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		Resource rs = new Resource<Integer>(project.getId());
		return new ResponseEntity<Resource>(rs, httpStatus);
    }
	
	private long formulateTimestamp(int days){
		// now UTC
		long millsPerDay = 86400000l;
		return System.currentTimeMillis() + (days*millsPerDay);
	}
	
	 /**
     * Place bid with buyer's name and contact
     *
     * @param 
     * @return project Id
     */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/bid", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Resource> createBid(@RequestBody ProjectModel requestProject, 
			HttpSession session) throws Exception {
		LOG.info("Request payload:" + MarshallingUtil.marshalToJson(requestProject));
		HttpStatus httpStatus = null;
		//
		// Validations
		//
		List<String> errors = new ArrayList<String>();
		String error = null;
		if(requestProject.getCurrency() == null || 
				(!requestProject.getCurrency().equals("$") 
						&& !requestProject.getCurrency().toLowerCase().equals("dollars"))){
			error = "Currency for bid must be $ or dollars.";
			LOG.error(error);
			errors.add(error);
		}
		if(requestProject.getBuyerBid() == null || requestProject.getBuyerBid().doubleValue() < 100){
			error = "Bid must not be null and over 99 dollars.";
			LOG.error(error);
			errors.add(error);
		}
		if (requestProject.getBuyerEmail() == null || requestProject.getBuyerEmail().isEmpty()){
			error = "Buyer email must not be null.";
			LOG.error(error);
			errors.add(error);
		}
		if (requestProject.getBuyerPassword() == null || requestProject.getBuyerPassword().isEmpty()){
			error = "Buyer password must not be null.";
			LOG.error(error);
			errors.add(error);
		}
		if (requestProject.getId() == null){
			error = "Project Id must not be null.";
			LOG.error(error);
			errors.add(error);
		}
		if(errors.size() > 0){
			RestErrorMessage errorMessage = new RestErrorMessage(VALIDATION_ERROR, errors);
			Resource<RestErrorMessage> resource = new Resource<RestErrorMessage>(false, errorMessage);
			return new ResponseEntity<Resource>(resource, HttpStatus.BAD_REQUEST);
		}
		//
		// Place bid
		//
		Integer bidStatus = null;
		String message = "";
		try {
			bidStatus = projectService.addBid(requestProject.getBuyerPassword(),
				requestProject.getBuyerEmail(), requestProject.getId(),
				requestProject.getBuyerBid());
		}catch (Exception e){
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		if(bidStatus.equals(ProjectService.TOO_LATE)){
			message = "Bid submitted for " + requestProject.getId().toString() + " was too late.";
			httpStatus = HttpStatus.NOT_IMPLEMENTED;
		}else if(bidStatus.equals(ProjectService.TOO_MUCH)){
			message = "Bid submitted for " + requestProject.getId().toString() + " was too high.";
			httpStatus = HttpStatus.NOT_IMPLEMENTED;
		}else if(bidStatus.equals(ProjectService.BID_SUBMITTED)){
			message = "Bid was submitted for " + requestProject.getId().toString();
			httpStatus = HttpStatus.OK;
		}
		Resource rs = new Resource<String>(message);
		return new ResponseEntity<Resource>(rs, httpStatus);
    }
	
    /**
     * Retrieve minimum bid by ProjectId
     *
     * @param id -primary key of target order
     * @return
     *
     */
    @ResponseBody
    @RequestMapping(value = "/project/{id}", method = RequestMethod.GET)
    public ResponseEntity<Resource> retrieveMinBidById(@PathVariable("id") Integer id,
    		HttpSession httpSession) throws Exception
    {
       	HttpStatus httpStatus = null;
    	Bid bid = null;
    	try {
    		bid = projectService.findLowestBidById(id);
    	}catch (BiddingOpenException e) {
    		RestErrorMessage errorMessage = new RestErrorMessage(BIDDING_STILL_OPEN_ERROR, "Bidding still open.");
			Resource<RestErrorMessage> resource = new Resource<RestErrorMessage>(false, errorMessage);
			httpStatus = HttpStatus.NOT_FOUND;
			return new ResponseEntity<Resource>(resource, httpStatus );
    	}
       	if(bid == null){
       		httpStatus = HttpStatus.NOT_FOUND;
       		LOG.info("No bids exists.");
       	}else {
       		httpStatus = HttpStatus.OK;
       	}
    	Resource<Bid> resource = new Resource<Bid> (bid);
		return new ResponseEntity<Resource>(resource, httpStatus);
    }

  
}
