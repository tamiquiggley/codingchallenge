package tamarak.core.dao;

import java.util.List;

import tamarak.core.entity.Bid;

/**
 *
 * @author TMcDonald
 */
public interface BidDao
{
    Bid findById(Integer projectId, Integer buyerId);
    
    List<Bid> findAllByProjectId(Integer projectId);

    Bid save(Bid bid);
    
    void deleteById(Integer projectId);
    
  
}
