package tamarak.core.dao;

import java.util.List;

import tamarak.core.entity.Customer;

/**
 *
 * @author T McDonald
 */
public interface CustomerDao
{

	  Customer findById(Integer id);
	  
	  Customer findByEmailPassword(String email, String password);
		  
	  Customer save(Customer product);
	  
	  void deleteById(Integer id);

}
