package tamarak.core.dao;

import tamarak.core.entity.Project;

/**
 *
 * @author TMcDonald
 */
public interface ProjectDao
{

    Project findById(Integer id);
    
    Project save(Project project);
    
    void deletebyId(Integer id);

}
