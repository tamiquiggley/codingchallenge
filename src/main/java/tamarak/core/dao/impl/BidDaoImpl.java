package tamarak.core.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import tamarak.core.dao.BidDao;
import tamarak.core.entity.Bid;

/**
 *
 * @author T. McDonald
 */
@Repository
public class BidDaoImpl implements BidDao
{

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(BidDaoImpl.class
        .getName());

    @Resource(name = "bids")
    private Map<Integer, List<Bid>> bidMap;
	
    @Override
	public Bid findById(Integer projuctId, Integer buyerId) {
		List<Bid> list = bidMap.get(projuctId);
		Bid rBid = null;
		for(Bid bid:list){
			if(bid.getBuyerId().equals(buyerId)){
				rBid = bid;
				break;
			}
		}
		return rBid;
	}

	@Override
	public List<Bid> findAllByProjectId(Integer projectId) {
		return  bidMap.get(projectId);
	}

	@Override
	public Bid save(Bid bid) {
		List<Bid> list = bidMap.get(bid.getProjectId());
		if(list == null){
			list = new ArrayList<Bid>();
		}
		list.add(bid);
		bidMap.put(bid.getProjectId(), list);
		return bid;
	}

	@Override
	public void deleteById(Integer projectId) {
		bidMap.remove(projectId);
	}
   
}
