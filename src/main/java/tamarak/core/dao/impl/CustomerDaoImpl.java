package tamarak.core.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import tamarak.core.dao.CustomerDao;
import tamarak.core.entity.Customer;



/**
 *
 * @author T. McDonald
 */
@Repository
public class CustomerDaoImpl implements CustomerDao
{

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(CustomerDaoImpl.class
        .getName());
    
    @Resource(name = "customers")
    private Map<Integer, Customer> customerMap;

	@Override
	public Customer findById(Integer id) {
		return customerMap.get(id);
	}
	
	@Override
	public Customer findByEmailPassword(String email, String password) {
		Customer customer = null;
		List<Customer> list = new ArrayList<Customer>(customerMap.values());
		for(Customer c:list){
			if(c.getEmail().equals(email) && c.getPassword().equals(password)){
				customer = c;
				break;
			}
		}
		return customer;
	}

	@Override
	public Customer save(Customer customer) {
		Customer localCustomer = null;
		localCustomer = findByEmailPassword(customer.getEmail(), customer.getPassword());
		if(localCustomer==null){
			customer.setId(getNextCustomerId());
			customerMap.put(customer.getId(), customer);
		}else {
			customer.setId(localCustomer.getId());
		}
		return customer;
	}
	
	private int getNextCustomerId(){
		Set<Integer> set = (Set<Integer>) customerMap.keySet();
		int max = 0;
		for(Integer val:set){
			if(val.intValue() > max){
				max = val.intValue();
			}
		}
		return max+1;
	}

	@Override
	public void deleteById(Integer id) {
		customerMap.remove(id);
		
	}

}
