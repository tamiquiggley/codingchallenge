package tamarak.core.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import tamarak.core.dao.ProjectDao;
import tamarak.core.entity.Project;



/**
 *
 * @author T. McDonald
 */
@Repository
public class ProjectDaoImpl implements ProjectDao
{

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ProjectDaoImpl.class
        .getName());
    
    @Resource(name = "projects")
    private Map<Integer, Project> projectMap;

	@Override
	public Project findById(Integer id) {
		return projectMap.get(id);
	}

	@Override
	public Project save(Project project) {
		if(project.getId() == null){
			project.setId(Integer.valueOf(generateNextProjectId()));
		}
		projectMap.put(project.getId(), project);
		return project;
	}
	
	private int generateNextProjectId(){
		int max = 0;
		Set<Integer> set = projectMap.keySet();
		for(Integer val:set){
			if(val > max){
				max = val;
			}
		}
		return max+1;
	}

	@Override
	public void deletebyId(Integer id) {
		projectMap.remove(id);
	}
}
