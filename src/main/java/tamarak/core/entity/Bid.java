package tamarak.core.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author tmcdonald
 */
@Entity(name = "bids")
public class Bid implements Serializable
{
    private static final long serialVersionUID = 1L;

    public Bid(){}
  
    @Id
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer projectId;
    
    @Id
    @Column(nullable = false)
    private Integer buyerId;
    
    @Column
    private Double bid;
    
    public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(Integer buyerId) {
		this.buyerId = buyerId;
	}

	public Double getBid() {
		return bid;
	}

	public void setBid(Double bid) {
		this.bid = bid;
	}
	
	@Override
    public int hashCode()
    {
        int hash = 0;
        hash += (projectId != null ? projectId.hashCode() : 0);
        hash += (buyerId != null ? buyerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bid))
        {
            return false;
        }
        Bid other = (Bid) object;
        if ((this.projectId == null && other.projectId != null) || (this.projectId != null && !this.projectId.equals(other.projectId)))
        {
            return false;
        }
       
        return true;
    }

    @Override
    public String toString()
    {
        return "tamarak.core.entity.bid[ productId=" + projectId + " buyerId=" + buyerId + " bid=" +
        		bid + " ]";
    }

}
