package tamarak.core.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author tmcdonald
 */
@Entity(name = "projects")
public class Project implements Serializable
{
    private static final long serialVersionUID = 1L;

    public Project(){}
  
    @Id
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer id;
    
    @Column
    private Integer sellerId;
    
    @Column
    private Double maxBudget;
    
    @Column
    private Long lastDateTime;
    
    @Column
    private String description;
    
    @Column 
    private Boolean closed;  // True is closed
    
    private Bid minBid;
       
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSellerId() {
		return sellerId;
	}

	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}

	public Double getMaxBudget() {
		return maxBudget;
	}

	public void setMaxBudget(Double maxBudget) {
		this.maxBudget = maxBudget;
	}

	public Long getLastDateTime() {
		return lastDateTime;
	}

	public void setLastDateTime(Long lastDateTime) {
		this.lastDateTime = lastDateTime;
	}

	public Bid getMinBid() {
		return minBid;
	}

	public void setMinBid(Bid minBid) {
		this.minBid = minBid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project))
        {
            return false;
        }
        Project other = (Project) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "tamarak.core.entity.Product[ id=" + id + " maxBudget=" + maxBudget + " lastDateTime= " +
        		lastDateTime + " minBid="  + minBid.toString() + " description=" + description + " ]";
    }

}
