package tamarak.core.model;

/**
 *
 * @author tmcdonald
 */
public class ProjectModel
{
    public ProjectModel(){}
  
    private Integer id;
    
    private Double maxBudget;
    
    private String currency; // $ or dollars
    
    private Integer bidOpenDays;
    
    private String description;
    
    private Double minBid;
    
    private String buyerEmail;
    
    private String buyerPassword;
    
    private Double buyerBid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getMaxBudget() {
		return maxBudget;
	}

	public void setMaxBudget(Double maxBudget) {
		this.maxBudget = maxBudget;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getBidOpenDays() {
		return bidOpenDays;
	}

	public void setBidOpenDays(Integer bidOpenDays) {
		this.bidOpenDays = bidOpenDays;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getMinBid() {
		return minBid;
	}

	public void setMinBid(Double minBid) {
		this.minBid = minBid;
	}

	public String getBuyerEmail() {
		return buyerEmail;
	}

	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}

	public String getBuyerPassword() {
		return buyerPassword;
	}

	public void setBuyerPassword(String buyerPassword) {
		this.buyerPassword = buyerPassword;
	}

	public Double getBuyerBid() {
		return buyerBid;
	}

	public void setBuyerBid(Double buyerBid) {
		this.buyerBid = buyerBid;
	}
	
	
	
}
