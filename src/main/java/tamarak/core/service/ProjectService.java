package tamarak.core.service;

import tamarak.core.entity.Bid;
import tamarak.core.entity.Project;
import tamarak.core.service.impl.BiddingOpenException;

/**
 *
 * @author Tamara McDonald
 */
public interface ProjectService
{
	public static Integer TOO_LATE = 1;
	public static Integer TOO_MUCH = 2;
	public static Integer BID_SUBMITTED = 0;
	public static Double MIN_BID = 100d;
	
    Bid findLowestBidById(Integer projectId) throws BiddingOpenException;

    Project save(Integer id, Double maxBudget, Long deadline, String description) throws Exception;
   
    Integer addBid(String name, String contact, Integer projectId, Double bid);
}
