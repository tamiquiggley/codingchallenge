package tamarak.core.service.impl;

public class BiddingOpenException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BiddingOpenException(String message) {
		super(message);
	}
}
