package tamarak.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tamarak.core.dao.BidDao;
import tamarak.core.dao.CustomerDao;
import tamarak.core.dao.ProjectDao;
import tamarak.core.entity.Bid;
import tamarak.core.entity.Customer;
import tamarak.core.entity.Project;
import tamarak.core.service.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService {
	
	@Autowired
    private BidDao bidDao;
    
    @Autowired
    private ProjectDao projectDao;
    
    @Autowired
    private CustomerDao customerDao;

	@Override
	public Bid findLowestBidById(Integer projectId) throws BiddingOpenException{
		Bid bid = null;
		Project project = projectDao.findById(projectId);
		if(project.getLastDateTime().longValue() < System.currentTimeMillis()){
		   if(project.getMinBid() == null){
			   bid = calculateMinimumBid(projectId);
			   project.setMinBid(bid);
		   }else {
			   bid = project.getMinBid();
		   }
		}else {
			throw new BiddingOpenException("Too early to find minimum bid.");
		}
		return bid;
	}

	private Bid calculateMinimumBid(Integer projectId){
		Bid bid = null;
		List<Bid> list = bidDao.findAllByProjectId(projectId);
		double min = 0;
		for(Bid b:list){
			if(min == 0){
				min = b.getBid().doubleValue();
				bid = b;
			}
			else if(b.getBid().doubleValue() < min){
				min = b.getBid().doubleValue();
				bid = b;
			}
		}
		if(min < MIN_BID){
			bid = null;
		}
		return bid;
	}

	@Override
	public Project save(Integer id, Double maxBudget, Long deadline, String description) throws Exception {
		Project project = new Project();
		project.setId(id);
      	project.setMaxBudget(maxBudget);
		project.setLastDateTime(deadline);
		project.setDescription(description);
		project = projectDao.save(project);
		return project;
	}
	
	@Override
	public Integer addBid(String email, String password, Integer projectId, Double bidValue) {
		Project project = projectDao.findById(projectId);
		// Check if bid is before the deadline
		if(System.currentTimeMillis() < project.getLastDateTime().longValue()){
			// Check if bid is less than the maximum budget
			if(bidValue < project.getMaxBudget()){
				Customer customer = new Customer();
				customer.setEmail(email);
				customer.setPassword(password);
				// Id is created in DAO if it doesn't exist
				customer = customerDao.save(customer);
				Bid bid = new Bid();
				bid.setBid(bidValue);
				bid.setProjectId(projectId);
				bid.setBuyerId(customer.getId());
				bidDao.save(bid); 
			}else {
				return TOO_MUCH;
			}
		}else {
			return TOO_LATE;
		}
		return BID_SUBMITTED;
	}

	

}
