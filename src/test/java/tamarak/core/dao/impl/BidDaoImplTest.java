package tamarak.core.dao.impl;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import tamarak.JUnitSpringTestBase;
import tamarak.core.dao.BidDao;
import tamarak.core.dao.CustomerDao;
import tamarak.core.entity.Bid;
import tamarak.core.entity.Customer;
import org.junit.runners.MethodSorters;

/**
 *
 * @author T.McDonald
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BidDaoImplTest extends JUnitSpringTestBase
{
    @Autowired
    private BidDao bidDao;
    
    @Autowired
    private CustomerDao customerDao;
    
   
    public BidDaoImplTest() {}
    
    @Test
    public void a_testCustomerAdd()
    {
    	System.out.println("Testing creating customers.");
    	Customer rCustomer;
    	Customer bean = new Customer();
    	bean.setPassword("AnnieMac");
    	bean.setEmail("anntqmac@gmail.com");
    	rCustomer = customerDao.save(bean);
    	System.out.println("customer id: " + rCustomer.getId());
    	//
        bean = new Customer();
    	bean.setPassword("ElenaMac");
    	bean.setEmail("emcdonald@gmail.com");
    	rCustomer = customerDao.save(bean);
    	System.out.println("customer id: " + rCustomer.getId());
    	//
    	bean = new Customer();
     	bean.setPassword("j1mmymac");
     	bean.setEmail("jimmymac@yahoo.com");
     	rCustomer = customerDao.save(bean);
     	System.out.println("customer id: " + rCustomer.getId());
     	//
     	bean = new Customer();
     	bean.setPassword("Rizzi2");
     	bean.setEmail("rkurtz@yahoo.com");
     	rCustomer = customerDao.save(bean);
     	System.out.println("customer id: " + rCustomer.getId());
     	//
     	Customer testCustomer = customerDao.findById(rCustomer.getId());
     	assertEquals(testCustomer.getId().intValue(),rCustomer.getId().intValue());
    }
    
    @Test
    public void b_testBid()
    {
        System.out.println("Testing bidding with the bidDao.");
        Bid bean = new Bid();
        bean.setBid(2000d);
        bean.setBuyerId(1);
        bean.setProjectId(1);
        bidDao.save(bean);
        //
        bean = new Bid();
        bean.setBid(3000d);
        bean.setBuyerId(2);
        bean.setProjectId(1);
        bidDao.save(bean);
        //
        bean = new Bid();
        bean.setBid(4000d);
        bean.setBuyerId(3);
        bean.setProjectId(1);
        bidDao.save(bean);
        //
        bean = new Bid();
        bean.setBid(500d);
        bean.setBuyerId(4);
        bean.setProjectId(1);
        bidDao.save(bean);
        //
        List<Bid> list = bidDao.findAllByProjectId(1);
        assertEquals(list.size(), 4);
      
    }
    
}   
