package tamarak.core.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import tamarak.JUnitSpringTestBase;
import tamarak.core.dao.ProjectDao;
import tamarak.core.entity.Project;

import org.junit.runners.MethodSorters;

/**
 *
 * @author T.McDonald
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectDaoImplTest extends JUnitSpringTestBase
{

    @Autowired
    private ProjectDao projectDao;
    
    static Integer projectId;
    static long MILLIS_PER_DAY = 86400000l;

    public ProjectDaoImplTest() {}
   
    @Test
    public void a_testSave()
    {
        System.out.println("Testing adding a project with the ProjectDao.");
        Project bean = new Project();
        bean.setDescription("Now is the time.");
        bean.setMaxBudget(Double.valueOf(1000d));
        bean.setLastDateTime(System.currentTimeMillis()+(10*MILLIS_PER_DAY));
        Project project = projectDao.save(bean);
        assertNotNull(project.getId());
        projectId = project.getId();
    }
    
	@Test
    public void b_testFindById()
    {
        System.out.println("Testing finding a project with the ProjectDao.");
        Project result = projectDao.findById(projectId);
        assertEquals(result.getDescription(),"Now is the time.");
        assertNotNull(result.getLastDateTime());
        assertEquals(result.getMaxBudget(),Double.valueOf(1000d));
    }
   
}
