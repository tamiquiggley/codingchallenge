
package tamarak.core.service.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


import tamarak.JUnitSpringTestBase;
import tamarak.core.dao.BidDao;
import tamarak.core.dao.CustomerDao;
import tamarak.core.dao.ProjectDao;
import tamarak.core.entity.Bid;
import tamarak.core.entity.Customer;
import tamarak.core.entity.Project;
import tamarak.core.service.ProjectService;

import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.FixMethodOrder;

/**
 *
 * @author tmcdonald
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectServiceImplTest extends JUnitSpringTestBase
{

    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private ProjectDao projectDao;
    
    @Autowired
    private CustomerDao customerDao;
    
    @Autowired
    private BidDao bidDao;
    
    static Integer projectId;
    
    public ProjectServiceImplTest() {}

    @Test
    public void a_testCreateProjects()
    {
    	System.out.println("Testing creation of projects with the ProjectService.");
    	Project project_1 = null;
    	Project project_2 = null;
    	try {
    		project_1 = projectService.save(null, 1500d, System.currentTimeMillis() + (2*3600000), "Now is the time1");
    	}catch (Exception e){
    		// ignore exception
    	}
        System.out.println("Project_1 id: " + project_1.getId()); 
        try {
    		project_2 = projectService.save(null, 1000d, System.currentTimeMillis() + (3*3600000), "Now is the time2");
    	}catch (Exception e){
    		// ignore exception
    	}
        System.out.println("Project_2 id: " + project_2.getId()); 
        assertEquals(project_1.getId().intValue()+1, project_2.getId().intValue());
        projectId = project_2.getId();  // carry over to b_testBid()
    }
    
    @Test
    public void b_testBid()
    {
       System.out.println("Testing bidding on a project with the projectService.");
       Integer returnInt;
	   returnInt = projectService.addBid("anntqmac@gmail.com", "password1", projectId, 400d);
	   assertEquals( ProjectService.BID_SUBMITTED.intValue(),returnInt.intValue());
	   //
	   returnInt = projectService.addBid("annttqmac@gmail.com", "password2", projectId, 4010d);  
	   assertEquals( ProjectService.TOO_MUCH.intValue(),returnInt.intValue());
	   //
	   returnInt = projectService.addBid("annttqmac@gmail.com", "password2", projectId, 403d);  
	   assertEquals( ProjectService.BID_SUBMITTED.intValue(),returnInt.intValue());
	   //
	   returnInt = projectService.addBid("annnttqmac@gmail.com", "password3", projectId, 402d);  
	   assertEquals(ProjectService.BID_SUBMITTED.intValue(),returnInt.intValue());
	   //
	   Project project = projectDao.findById(projectId);
	   assertNotNull(project);
	   //
	   List<Bid> list = bidDao.findAllByProjectId(projectId);
	   assertEquals(list.size(),3);
	   //
	   System.out.println("Testing finding the lowest bid on a project.");
	   project.setLastDateTime(System.currentTimeMillis()-(2*3600000));
	   projectDao.save(project);
	   //
	   returnInt = projectService.addBid("annntttqmac@gmail.com", "password4", projectId, 402d);  
	   assertEquals(ProjectService.TOO_LATE.intValue(),returnInt.intValue());
	   //
	   Bid bid = null;
	   try {
		   bid = projectService.findLowestBidById(projectId);
	   }catch (BiddingOpenException e){
		   // ignore
	   }
	   Double amount = bid.getBid();
	   Integer customerId = bid.getBuyerId();
	   assertEquals(Double.valueOf(400d), amount);
	   //
	   Customer customer = customerDao.findById(customerId);
	   assertEquals(customer.getEmail(), "anntqmac@gmail.com");
	   //
    }

}
