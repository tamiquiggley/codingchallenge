#!/bin/bash
echo "**************************************************************"
echo " This is an interactve test script for the Job Bid APIs "
echo "**************************************************************"
echo ""
echo ""
echo "First, create various projects as follows:"
echo " 1) Create three different projects with 2 days open."
echo " 2) Create three bids on each project for a total of 9 bids."
echo " 3) Adjust the close date on all three projects to be now."
echo " 3) Get the lowest bid, and customer for each project."
echo ""
#
# TEST 1
#
read -r -p "Create three projects? [Y/n] " input
case $input in 
	[yY][eE][sS]|[yY])
echo "TEST 1"
curl -H "Content-Type: application/json" -X POST -d @payload1.json http://localhost:8080/jobbid/project | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @payload2.json http://localhost:8080/jobbid/project | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @payload3.json http://localhost:8080/jobbid/project | python -mjson.tool
;;
	[nN][oO]|[nN])
echo "TEST 1 no"
	;;
*)
echo "Invalid input..."
	exit 1
;;
esac
echo ""
echo ""
#
# TEST 2
#
read -r -p "Create 3 bids on each project? [Y/n] " input
case $input in 
	[yY][eE][sS]|[yY])
echo "TEST 2"
curl -H "Content-Type: application/json" -X POST -d @bidpayload1.json http://localhost:8080/jobbid/bid | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @bidpayload2.json http://localhost:8080/jobbid/bid | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @bidpayload3.json http://localhost:8080/jobbid/bid | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @bidpayload4.json http://localhost:8080/jobbid/bid | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @bidpayload5.json http://localhost:8080/jobbid/bid | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @bidpayload6.json http://localhost:8080/jobbid/bid | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @bidpayload7.json http://localhost:8080/jobbid/bid | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @bidpayload8.json http://localhost:8080/jobbid/bid | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @bidpayload9.json http://localhost:8080/jobbid/bid | python -mjson.tool
;;
	[nN][oO]|[nN])
echo "TEST 2 no"
	;;
*)
echo "Invalid input..."
	exit 1
;;
esac
echo ""
echo ""
#
# TEST 3
#
read -r -p "Close out all three projects? [Y/n] " input
case $input in 
	[yY][eE][sS]|[yY])
echo "TEST 3"
curl -H "Content-Type: application/json" -X POST -d @closeoutpayload1.json http://localhost:8080/jobbid/project | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @closeoutpayload2.json http://localhost:8080/jobbid/project | python -mjson.tool
curl -H "Content-Type: application/json" -X POST -d @closeoutpayload3.json http://localhost:8080/jobbid/project | python -mjson.tool
;;
	[nN][oO]|[nN])
echo "TEST 3 no"
	;;
*)
echo "Invalid input..."
	exit 1
;;
esac
#
# TEST 4
#
read -r -p "Run find lowest bid by id? [Y/n] " input
case $input in 
	[yY][eE][sS]|[yY])
echo "TEST 4"
curl -H "Content-type: application/json" -X GET http://localhost:8080/jobbid/project/1| python -mjson.tool
curl -H "Content-type: application/json" -X GET http://localhost:8080/jobbid/project/2| python -mjson.tool
curl -H "Content-type: application/json" -X GET http://localhost:8080/jobbid/project/3| python -mjson.tool
;;
	[nN][oO]|[nN])
echo "TEST 4 no"
	;;
*)
echo "Invalid input..."
	exit 1
;;
esac
echo ""
echo ""

